#!/bin/bash

for ippwd in `cat $1`;do
  str=$ippwd
  delimiter=:
  s=$str$delimiter
  array=();
  while [[ $s ]]; do
    array+=( "${s%%"$delimiter"*}" );
    s=${s#*"$delimiter"};
  done;
  ip=${array[0]}
 
  ping $ip -c 3

done

